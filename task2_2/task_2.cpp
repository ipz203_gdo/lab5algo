﻿#include<conio.h>
#include<stdlib.h>
#include<time.h>
#include <stdio.h>
#include <chrono>
#include <time.h>
#define MAX_N 100000

int main()
{
	int short n;
	int arr[MAX_N];
	printf("n = ");
	scanf_s("%d", &n);
	float dd;
	clock_t start, finish;
	start = clock();
	srand(time(0));
	if (n < 1 || n > MAX_N)
	{
		printf("You make a mistake when entering the number n!\n");
		return 0;
	}
	printf("arr = {");
	for (int i = 0; i < n; i++)
	{
		arr[i] = rand() % 21 - 10;
		printf("%d", arr[i]);
		if (i != n - 1)
			printf(", ", arr[i]);
	}
	printf("}\n");
	int tmp, is, j, i;
	for (i = 1; i < n; i++)
	{
		tmp = arr[i];
		for (j = i - 1; j >= 0 && arr[j] > tmp; j--)
			arr[j + 1] = arr[j];
		arr[j + 1] = tmp;
	}
	printf("arr = {");
	for (int i = 0; i < n; i++)
	{
		printf("%d", arr[i]);
		if (i != n - 1)
			printf(", ", arr[i]);
	}
	printf("}\n");
	finish = clock();
	dd = (float)(finish - start) / CLOCKS_PER_SEC;
	printf("time = %f", dd);
	_getch();
	return 0;
}
